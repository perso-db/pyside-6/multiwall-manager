from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader
from PySide2 import QtGui, QtWidgets, QtCore
from ui.wallpaper_widget import WallpaperWidget
from wallpaper.changer import ChangerThread
from config.config import config
from wallpaper.wallpaper import Wallpaper


class MainWindow(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.changer_thread = None
        self.icon = None
        self.tray_icon = None

    def finish_init(self):
        self.setWindowTitle("Wallpaper Changer")
        self.add_thread()
        self.connect_buttons()
        self.add_icons()
        self.setup_config_parameters()

    def add_thread(self):
        changer_thread = ChangerThread()
        changer_thread.signals.wallpapers_changed.connect(self.wallpapers_changed)
        changer_thread.signals.status_changed.connect(self.status_changed)
        self.changer_thread = changer_thread
        self.changer_thread.start()

    def connect_buttons(self):
        self.stop_button.clicked.connect(self.changer_thread.stop)
        self.start_button.clicked.connect(self.changer_thread.start)

    def add_icons(self):
        icon_path = config.app_folder / "binaries" / "Wallpaper_by_DB.xpm"
        self.icon = QtGui.QIcon(str(icon_path))
        self.tray_icon = QtWidgets.QSystemTrayIcon(self.icon, self)
        tray_signal = "activated(QSystemTrayIcon::ActivationReason)"
        QtCore.QObject.connect(self.tray_icon, QtCore.SIGNAL(tray_signal), self.icon_activated)
        if config.toml["start_in_tray"]:
            self.tray_icon.show()
        else:
            self.show()

    def setup_config_parameters(self):
        self.dir_edit.setText(config.toml["wallpaper_folder"])
        self.frequence_edit.setText(str(config.toml["changing_period"]))
        self.dir_button.clicked.connect(self.select_wallpaper_folder)
        self.apply_button.clicked.connect(self.save_config)

    def select_wallpaper_folder(self):
        new_dir = QtWidgets.QFileDialog.getExistingDirectory(
            parent=None,
            caption="Choisir un répertoire",
            dir=self.dirEdit.text(),
            options=QtWidgets.QFileDialog.ShowDirsOnly)
        self.dir_edit.setText(new_dir)

    def save_config(self):
        config.save(changing_period=self.frequence_edit.text(),
                    wallpaper_folder=self.dir_edit.text())
        Wallpaper.get_wallpapers()

    def changeEvent(self, event):
        if event.type() == QtCore.QEvent.WindowStateChange:
            if self.windowState() & QtCore.Qt.WindowMinimized:
                self.tray_icon.show()
                self.hide()
                event.ignore()

    def icon_activated(self, reason):
        if reason == QtWidgets.QSystemTrayIcon.DoubleClick:
            self.show()
            self.setWindowState(QtCore.Qt.WindowNoState)
            self.tray_icon.hide()

    def wallpapers_changed(self, wallpapers):
        for i in reversed(range(self.wallpaper_layout.count())):
            self.wallpaper_layout.itemAt(i).widget().setParent(None)
        for wallpaper in wallpapers:
            wallpaper_widget = WallpaperWidget.create_wallpaper_widget(self, wallpaper)
            self.wallpaper_layout.addWidget(wallpaper_widget)

    def status_changed(self, status):
        self.state_label.setText(status)

    @staticmethod
    def create_main_window():
        loader = QUiLoader()
        loader.registerCustomWidget(MainWindow)
        ui_file = QFile(str(config.app_folder / "ui/main_window.ui"))
        ui_file.open(QFile.ReadOnly)
        window = loader.load(ui_file)
        ui_file.close()
        window.finish_init()
        return window
