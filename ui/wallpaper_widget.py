from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2.QtUiTools import QUiLoader
from config.config import config
from pathlib import Path


class WallpaperWidget(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = ""
        self.main_window = None

    def finish_init(self, main_window, name):
        self.name = name
        self.main_window = main_window
        self.name_label.setText(self.name)
        self.delete_button.clicked.connect(lambda: self.move_to_folder("A supprimer"))
        self.analyse_button.clicked.connect(lambda: self.move_to_folder("A analyser"))

    def move_to_folder(self, folder_name):
        wallpaper_folder_path = Path(config.toml["wallpaper_folder"])
        wallpaper_file = wallpaper_folder_path / self.name
        new_folder = wallpaper_folder_path / folder_name
        new_folder.mkdir(parents=True, exist_ok=True)
        wallpaper_file.rename(new_folder / self.name)
        self.main_window.changer_thread.restart()

    @staticmethod
    def create_wallpaper_widget(main_window, name):
        loader = QUiLoader()
        loader.registerCustomWidget(WallpaperWidget)
        ui_file = QtCore.QFile(str(config.app_folder / "ui/wallpaper_widget.ui"))
        ui_file.open(QtCore.QFile.ReadOnly)
        widget = loader.load(ui_file)
        ui_file.close()
        widget.finish_init(main_window, name)
        return widget
