#! C:\Users\CassanR\Miniconda3\envs\wallpaper\python.exe
# -*- coding: utf-8 -*-
"""
Launches the app.
"""
import sys
from PySide2.QtWidgets import QApplication
from pathlib import Path
import toml
from config.config import config, write_to_log
from ui.main_window import MainWindow


def init():
    if getattr(sys, 'frozen', False):
        app_folder = Path(sys.executable).parent
    else:
        import __main__
        app_folder = Path(__main__.__file__).parent.absolute()
    toml_path = app_folder / "config" / "config.toml"
    config.toml = toml.load(str(toml_path.absolute()))
    config.app_folder = app_folder
    config.border_path = str((app_folder / "temp" / "border.jpg").absolute())
    config.black_path = str((app_folder / "temp" / "black.jpg").absolute())
    config.wallpaper_path = str((app_folder / "wallpaper.jpg").absolute())
    log_file = open(app_folder / "log.txt", "a")
    # sys.stdout = log_file
    # sys.stderr = log_file
    write_to_log("\n\n###################\n\n")


def main():
    init()
    app = QApplication(sys.argv)
    window = MainWindow.create_main_window()
    app.setWindowIcon(window.icon)
    sys.exit(app.exec_())


if __name__ == '__main__':
    # try:
    main()
    # except Exception as e:
    #     with open(config["app_folder"] / "log.txt", "a") as log_file:
    #         import logging
    #         sys.stdout = log_file
    #         sys.stderr = log_file
    #         logging.exception("Something awful happened!")
