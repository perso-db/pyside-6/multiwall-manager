from dataclasses import dataclass
from pathlib import Path
import toml


@dataclass
class Config:
    toml = {}
    app_folder = Path(".")
    border_path = ""
    black_path = ""
    wallpaper_path = ""
    number_of_screens = 0

    def save(self, changing_period, wallpaper_folder):
        toml_path = self.app_folder / "config" / "config.toml"
        data = ""
        with open(toml_path, "r") as toml_file:
            for line in toml_file.readlines():
                if line.startswith("changing_period"):
                    data += f"changing_period = {changing_period}\n"
                elif line.startswith("wallpaper_folder"):
                    data += f'wallpaper_folder = "{wallpaper_folder}"\n'
                else:
                    data += line
        with open(toml_path, "w") as toml_file:
            toml_file.write(data)
        self.toml = toml.load(str(toml_path.absolute()))


config = Config()


def write_to_log(msg):
    with open(config.app_folder / "log.txt", "a") as log_file:
        log_file.write(msg)
        log_file.write("\n")
