from PIL import Image

from config.config import config, write_to_log


class MyImage:

    SIDES = 1
    TOP_DOWN = 2
    LEFT = 3
    BOTTOM = 4

    def __init__(self, image, name):
        self.image = image
        self.name = name
        self.border_position = None

    @property
    def size(self):
        return self.image.size

    def covered_screens(self):
        width, height = self.image.size
        ratio = width / height
        target_ratio = config.toml["target_width"] / config.toml["target_height"]
        return max(1, round(ratio / target_ratio))

    def target_size(self):
        return config.toml["target_width"] * self.covered_screens(), config.toml["target_height"]

    def get_transformed_image(self):
        target_width, target_height = self.target_size()
        target_ratio = target_width / target_height
        image_width, image_height = self.image.size
        image_ratio = image_width / image_height
        if target_ratio > image_ratio:
            transform_ratio = target_height / image_height
        else:
            transform_ratio = target_width / image_width
        new_height = int(image_height * transform_ratio)
        new_width = int(image_width * transform_ratio)
        try:
            new_image = self.image.resize((new_width, new_height))
        except (ValueError, OSError):
            write_to_log(f"target_size : {self.target_size()}")
            write_to_log(f"image_name : {self.name}")
            write_to_log(f"image_size : {self.image.size}")
            write_to_log(f"target_ratio : {target_ratio}")
            write_to_log(f"image_ratio : {image_ratio}")
            write_to_log(f"new_width : {new_width}")
            write_to_log(f"new_height : {new_height}")
            return None
        return new_image

    def create_borders(self):
        image_width, image_height = self.image.size
        target_width, target_height = self.target_size()
        if image_width < target_width - 1:
            self.border_position = self.SIDES
            border_size = (int((target_width - image_width) / 2), image_height)
        elif image_width == target_width - 1:
            self.border_position = self.LEFT
            border_size = (1, image_height)
        elif image_height < target_height - 1:
            self.border_position = self.TOP_DOWN
            border_size = (target_width, int((target_height - image_height) / 2))
        elif image_height == target_height - 1:
            self.border_position = self.BOTTOM
            border_size = (image_width, 1)
        else:
            return None
        try:
            Image.open(config.black_path).resize(border_size).save(config.border_path)
        except ValueError:
            write_to_log(f"target_size : {self.target_size()}")
            write_to_log(f"image_name : {self.name}")
            write_to_log(f"image_size : {self.image.size}")
            write_to_log(f"border_size : {border_size}")
            exit()

    def get_image_with_borders(self):
        self.create_borders()
        if not self.border_position:
            return self.image
        final_image = Image.new('RGB', self.target_size())
        border = Image.open(config.border_path)
        border_width, border_height = border.size
        image_width, image_height = self.image.size
        final_image.paste(border, (0, 0))
        if self.border_position == self.SIDES:
            final_image.paste(self.image, (border_width, 0))
            final_image.paste(border, (border_width + image_width, 0))
        if self.border_position == self.TOP_DOWN:
            final_image.paste(self.image, (0, border_height))
            final_image.paste(border, (0, border_height + image_height))
        return final_image
