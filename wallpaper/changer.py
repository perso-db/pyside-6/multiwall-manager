import ctypes
import time
from config.config import config
from wallpaper.wallpaper import Wallpaper
from PySide2 import QtCore


class ChangerThread(QtCore.QThread):

    def __init__(self):
        super().__init__()
        self.signals = ChangeWorkerSignals()
        self.is_running = False
        self.start_loop_time = 0

    @QtCore.Slot()
    def run(self):
        self.is_running = True
        self.signals.status_changed.emit("Running")
        self.start_loop_time = time.time()
        self.change_wallpaper()
        while self.is_running:
            delta = time.time() - self.start_loop_time
            if delta >= config.toml["changing_period"] * 60:
                self.change_wallpaper()
                self.start_loop_time = time.time()
            time.sleep(1)

    def stop(self):
        self.is_running = False
        self.signals.status_changed.emit("Stopped")

    def restart(self):
        self.is_running = False
        while self.isRunning():
            pass
        self.start()

    @staticmethod
    def get_number_of_screens():
        user32 = ctypes.windll.user32
        screen_size = user32.GetSystemMetrics(78), user32.GetSystemMetrics(79)
        number_of_screens = ChangerThread.number_of_screens_from_screen_size(screen_size)
        config.number_of_screens = number_of_screens

    @staticmethod
    def number_of_screens_from_screen_size(screen_size):
        target_ratio = config.toml["target_width"] / config.toml["target_height"]
        screen_height, screen_width = screen_size
        screen_ratio = screen_height / screen_width
        return round(screen_ratio / target_ratio)

    def change_wallpaper(self):
        ChangerThread.get_number_of_screens()
        wallpaper = Wallpaper()
        wallpaper.choose_random_images()
        wallpaper.create()
        ctypes.windll.user32.SystemParametersInfoW(20, 0, config.wallpaper_path, 0)
        self.signals.wallpapers_changed.emit([image.name for image in wallpaper.images])


class ChangeWorkerSignals(QtCore.QObject):

    wallpapers_changed = QtCore.Signal(list)
    status_changed = QtCore.Signal(str)
