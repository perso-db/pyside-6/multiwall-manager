import random
from pathlib import Path
from PIL import Image
from config.config import config
from wallpaper.my_image import MyImage


class Wallpaper:

    wallpapers = []

    def __init__(self):
        self.images = []
        self.covered_screens = 0

    @staticmethod
    def get_wallpapers():
        wallpaper_folder = Path(config.toml["wallpaper_folder"])
        Wallpaper.wallpapers = [file for file in wallpaper_folder.iterdir()
                                if file.is_file() and file.name != "Thumbs.db"]

    def choose_random_images(self):
        while self.covered_screens < config.number_of_screens:
            try:
                wallpaper = random.choice(Wallpaper.wallpapers)
            except IndexError:
                self.get_wallpapers()
                wallpaper = random.choice(Wallpaper.wallpapers)
            image = MyImage(Image.open(wallpaper), wallpaper.name)
            screens_to_cover = config.number_of_screens - self.covered_screens
            covered_screens = image.covered_screens()
            if covered_screens <= screens_to_cover:
                self.covered_screens += covered_screens
                self.images.append(image)
                Wallpaper.wallpapers.remove(wallpaper)

    def transform_images(self):
        images = self.images
        self.images = []
        for image in images:
            self.images.append(MyImage(image.get_transformed_image(), image.name))

    def create(self):
        self.transform_images()
        self.add_borders()
        self.join_images()

    def join_images(self):
        screen_size = config.toml["target_width"] * config.number_of_screens, config.toml["target_height"]
        wallpaper = Image.new('RGB', screen_size)
        left, top = 0, 0
        for image in self.images:
            box = (left, top)
            wallpaper.paste(image.image, box)
            width, _ = image.size
            left += width
        wallpaper.save(config.wallpaper_path)

    def add_borders(self):
        images = self.images
        self.images = []
        for image in images:
            self.images.append(MyImage(image.get_image_with_borders(), image.name))
